upperTriangle = function(A){
	if (dim(A)[1] <= dim(A)[2])
	{
		for (i in 1 : (dim(A)[1]-1))
		{
			for (j in (i + 1) : (dim(A)[1]))
			{
				if (A[i,i] != 0)
				{
					c = A[j,i]/A[i,i]
					if (c != 0)
					{
						A[j,] = A[j,] - c * A[i,]
					}
				}
			} 
		}
	}

	else
	{
		for (i in 1:dim(A)[2])
		{
			for (j in (i+1):dim(A)[1])
			{
				if (A[i,i]!=0)
				{
					c = A[j,i]/A[i,i]
					if (c != 0)
					{
						A[j,] = A[j,] - c * A[i,]					
					}
				}
			}
		}
	}
	return(A)
}