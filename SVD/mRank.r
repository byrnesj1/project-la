mRank = function(A)
{
	source("upperTriangle.r")
	U = upperTriangle(A)
	locations = matrix(0,nrow = dim(A)[1])
	for (i in 1:dim(A)[1])
	{
		if (sum(U[i,]^2) != 0)
		{
			locations[i] = min(which(U[i,] != 0))
		}
	}

	rank = length(which(locations != 0))
	nullity = max(nrow(A),ncol(A)) - rank
	
	d = c(rank,nullity)
	names(d) = c("rank","nullity")
	return(d)
}