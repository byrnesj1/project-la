library(graphics)
library(jpeg)
library(pixmap)


highResComp = function(i, img)
{
	
r.matrix <- matrix(img@red, nrow = img@size[1], ncol = img@size[2])
g.matrix <- matrix(img@green, nrow = img@size[1], ncol = img@size[2])
b.matrix <- matrix(img@blue, nrow = img@size[1], ncol = img@size[2])


r.svd <- svd(r.matrix)
g.svd <- svd(g.matrix)
b.svd <- svd(b.matrix)

r.d <- r.svd$d
r.u <- r.svd$u
r.v <- r.svd$v

g.d <- g.svd$d
g.u <- g.svd$u
g.v <- g.svd$v

b.d <- b.svd$d
b.u <- b.svd$u
b.v <- b.svd$v



r.compressed <- r.u[,1:i] %*% diag(r.d[1:i]) %*% t(r.v[,1:i])
g.compressed <- g.u[,1:i] %*% diag(g.d[1:i]) %*% t(g.v[,1:i])
b.compressed <- b.u[,1:i] %*% diag(b.d[1:i]) %*% t(b.v[,1:i])


r.compressed[which(r.compressed < 0)] =0
g.compressed[which(g.compressed < 0)] =0
b.compressed[which(b.compressed < 0)] =0

r.compressed[which(r.compressed > 1)] =1
g.compressed[which(g.compressed > 1)] =1
b.compressed[which(b.compressed > 1)] =1




if (i <= 9)
{
	p = paste("00",i, sep = "")
}

else if (i > 9 & i <= 99)
{
	p = paste("0",i,sep="")
}

else 
{
	p = i
}


filename = paste("higres",p,sep="")
out = pixmapRGB(c(r.compressed,g.compressed,b.compressed),nrow = nrow(img@red),ncol=ncol(img@red))
write.pnm(out,filename,type="ppm")



#tiff(filename,width = 1920,height = 1080)
#title = paste("Compressed Image using", i,"singular values")
#plot(1:2,type='n',xaxt='n',yaxt='n',main = title,ylab = "",xlab="")
#rasterImage(compressedImg, 1.1,1,1.9,2)
#dev.off()


}

