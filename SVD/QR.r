QR = function(A)
{
	##QR Factorization (Pg 403 - 404 - Poole)

	source("transpose.r")
	source("GS.r")
	source("mRank.r")


	Q = GS(A)
	R = transpose(Q[[1]]) %*% A

	L = list(Q[[1]],R)
	


	return(L)

}