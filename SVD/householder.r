HH = function(A)
{

	source("transpose.r")
	source("len.r")

	n = nrow(A)
	I = diag(1,n)
	Q = I
	B = A
	
	m = min(nrow(A),ncol(A))

	for (i in 1:(m-1))
	{
		a = B[,1]
		e = rep(0,length(a))
		e[1] = 1
		v = a - sign(B[1,1])* len(a) * e
		
		d = diag(1,nrow(B))
		
		if (len(v) == 0)
		{
			h = d
		}
		else
		{
			a = (t(v)%*%v)
			s = 2/a
			p = as.numeric(s)
			x = v %*% t(v)



			h =d-  p*x 
		}

		H = I
		H[i:n,i:n] = h
		Q = Q %*% t(H)

		B = h %*% B
		B = B[-1,-1]
	}

	R = t(Q) %*% A
	return(list(Q,R))

}