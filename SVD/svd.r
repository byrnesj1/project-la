SVD = function(A)
{
	source("eig.r")
	source("GS.r")
	source("transpose.r")

	B = transpose(A) %*% A


	x = eig(B)
	ord = order(x[[1]],decreasing = T)
	sv = sqrt(x[[1]][ord])
	V = x[[2]][,ord]

	sigma = matrix(0,nrow = nrow(A),ncol = ncol(A))

	diag(sigma) = sv[1:mRank(A)[[1]]]



	U = matrix(0,nrow = nrow(A),ncol = nrow(A))

	for (i in 1:nrow(U))
	{
		
		if (ncol(V) < nrow(U))
		{
			for (j in 1:ncol(V))
			{
				U[,i] = (1 / sv[i])*A%*%V[,j]
			}

		
		}

		else 
		{
			U[,i] = (1 / sv[i]*A%*%V[,i])
		}


	}

	if (ncol(V)<nrow(U))
	{
		U = GS(U)
	}


	decomp = list(U,sigma,V)
	names(decomp) = c("U","sigma","V")

	return(decomp)
}







#	x = eig(A)
#	sigma = diag(x[[1]])
#	V = x[[2]]
#
#	AV = A %*% V
#
#	U = matrix(0,nrow = nrow(A),ncol = ncol(A))
#	for (i in 1:ncol(A))
#	{
#		U[,i] = AV[,i] / len(AV[,i])
#	}
#	
#
#	return(list(U,sigma,V))
#
#}
