transpose = function(A)
{
	T = matrix(0,nrow = dim(A)[2], ncol = dim(A)[1])

	for (i in 1:nrow(A))
	{
		T[,i] = A[i,]
	}

	return(T)

}