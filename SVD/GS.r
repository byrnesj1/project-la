GS = function(A)
{
	##Gram-Schmidt alogirthm to produce othonormal basis of matrix A (pg 400 - Poole)


	source("len.r")
	source("transpose.r")
	source("mRank.r")
	
	v = matrix(0, nrow = dim(A)[1],ncol = dim(A)[2])
	v[,1] = A[,1]
	for (i in 2:dim(A)[2])
	{
		sum = 0
		for (j in 1:(i-1))
		{
			if (len(v[,j]) != 0)
			{
				c = ((v[,j] %*% A[,i]) / (v[,j] %*% v[,j])) * v[,j] 
			}
			else
			{
				c = ((v[,j] %*% A[,i])) * v[,j]
			}

			sum = sum + c
		}
		v[,i] = A[,i] - sum
	}
	
	for (i in 1:dim(v)[2])
	{
		if (len(v[,i]) != 0)
		{
			v[,i] = v[,i] / len(v[,i])
		}
	}

	return(list(v, transpose(v)%*%A))
}



