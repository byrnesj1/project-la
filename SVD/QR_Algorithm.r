QR_Algorithm = function(A)
{
	source("mRank.r")
	source("householder.r")
	source("GS.r")

	if (mRank(A)[2] == 0)
	{
		decomp = "G"
	}
	else
	{
		decomp = "H"
	}


	if (decomp == "G")
	{
		res = GS(A)
	}

	else
	{
		res = HH(A)
	}

	vA = diag(1, dim(A)[1])
	vA = vA %*% res[[1]]
	e = 1

	l = nrow(A) ^3
	while (e > l * 0.000000000000000001)
	{
	
		B = res[[2]] %*% res[[1]]

		if (decomp == "G")
		{
			res = GS(B)
		}
		else
		{
			res = HH(B)
		}
		vA = vA %*% res[[1]]
		C = res[[2]] %*% res[[1]]
	
		e = sum((diag(C) - diag(B))^2)
	}


	return(list(diag(C), vA%*%res[[1]]))
}